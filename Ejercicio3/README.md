**Laboratorio 1 Unidad 1**                                                                                             
Programa basado en programación orientada a objetos y arreglos.                                                                           

**Pre-requisitos**                                                                                                                                    
Cplusplus                                                                                                                                                
Make                                                                                                                                             
Ubuntu                                                                                                                                                                                                                                                                            
**Instalación**                                                                                                                                       
Instalar Make si no estan en su computador.                                

Para instalar Make inicie la terminal y escriba lo siguiente:

sudo apt install make

**Ejecutando**                                                                                                                                         
Para abrir el programa necesita ejecutar el archivo make desde la terminal, luego escribir en la terminal lo siguiente: ./Programa
Una vez abierto el programa le pedira ingresar el numero de clientes, luego de eso debera ingresar los datos del cliente y al finalizar el programa le dara los datos y le dira si la persona es morosa o no.

**Construido con**                                                                                                                                    
C++
Librerias:
Iostream
List                                                                                                                               

**Versionado**                                                                                                                                        
Version 1.2                                                                                                                                       

**Autores**                                                                                                                                                                                                                                                 
Rodrigo Valenzuela                                                                                                                                                                                               