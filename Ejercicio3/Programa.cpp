#include <iostream>
#include "Empresa.h"
using namespace std;

int main()
{
  // Se ingresa el tamaño del arreglo. 
  int tamano;
  cout << "Ingrese el numero de clientes: " << endl;
  cin >> tamano;
  
  // Creacion del arreglo con el tamaño.
  Empresa arreglo[tamano];
  string nombre;
  string telefono;
  int saldo;
  bool moroso;
  
  // For que permite el ingreso de las personas.
  for (int i=0; i<tamano; i++) {
    cout << "Ingrese el nombre del "<< i+1 << "° cliente: " << endl;
    cin >> nombre;
    // Set que guarda el nombre.
    arreglo[i].set_nombre(nombre);
  }
  
  // For que permite el ingreso del telefono.
  for (int i=0; i<tamano; i++){
    cout << "Ingrese el telefono del "<< i+1 << "° cliente: " << endl;
    cin >> telefono;
    // Set que guarda el telefono.
    arreglo[i].set_telefono(telefono);
  } 
  
  // For que permite el ingreso del saldo.
  for (int i=0; i<tamano; i++){
    cout << "Ingrese el saldo del "<< i+1 << "° cliente: " << endl;
    cin >> saldo;
    // Set que guarda el valor.
    arreglo[i].set_saldo(saldo);
  } 
  // For que pregunta morosidad.
  for (int i=0; i<tamano; i++){
    cout << "¿El cliente "<< i+1 << "° es moroso? 1=Si/0=no" << endl;
    cin >> moroso;
    // Set que guarda la morosidad.
    arreglo[i].set_moroso(moroso);
  } 
  
  // For para la impresion de los datos.
  for (int i=0; i<tamano; i++){
    cout << "--------------------------------------------" << endl;
    cout << "Nombre: " << arreglo[i].get_nombre() << endl;
    cout << "Telefono: "<< arreglo[i].get_telefono() << endl;
    cout << "Saldo: " <<arreglo[i].get_saldo() << endl;
    // Condicion si el usuario ingreso 1 en morosidad.
    if(arreglo[i].get_moroso() == 1){
      cout << arreglo[i].get_nombre() << " es moroso." << endl;
    }
    // Condicion si el usuario ingreso 0 en morosidad.
    if(arreglo[i].get_moroso() == 0){
      cout << arreglo[i].get_nombre() << " no es moroso." << endl;
    }
    cout << "--------------------------------------------" << endl;
  }
  return 0;
}

