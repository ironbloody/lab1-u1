#include <iostream>
using namespace std;

#ifndef EMPRESA_H
#define EMPRESA_H

class Empresa {
  
  // Metodos privados
  private:
    string nombre = "";
    string telefono = "";
    int saldo = 0;
    bool moroso;
  
  // Metodos publicos
  public:
    Empresa ();
    Empresa (string nombre, string telefono, int saldo, bool moroso);
    
    // Set and Get
    void set_nombre(string arreglo);
    void set_telefono(string telefono);
    void set_saldo(int saldo);
    void set_moroso(bool moroso);
    
    string get_nombre();
    string get_telefono();
    int get_saldo();
    bool get_moroso();
};
#endif  