#include <iostream>
#include "Empresa.h"
using namespace std;

Empresa::Empresa(){
  string nombre = "";
  string telefono = "";
  int saldo = 0;
  bool moroso;
}

Empresa::Empresa(string nombre, string telefono, int saldo, bool moroso){
  this -> nombre = nombre;
  this -> telefono = telefono;
  this -> saldo = saldo;
  this -> moroso = moroso;
}

// Set and Get
void Empresa::set_nombre(string nombre){
  this -> nombre = nombre;
}

void Empresa::set_telefono(string telefono){
  this -> telefono = telefono;
}

void Empresa::set_saldo(int saldo){
  this -> saldo = saldo;
}

void Empresa::set_moroso(bool moroso){
  this -> moroso = moroso;
}

string Empresa::get_nombre(){
  return this -> nombre;
}

string Empresa::get_telefono(){
  return this -> telefono;
}

int Empresa::get_saldo(){
  return this -> saldo;
}

bool Empresa::get_moroso(){
  return this -> moroso;
}