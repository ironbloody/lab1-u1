**Laboratorio 1 Unidad 1**                                                                                             
Programa basado en programación orientada a objetos y arreglos.                                                                           

**Pre-requisitos**                                                                                                                                    
Cplusplus                                                                                                                                                
Make                                                                                                                                             
Ubuntu                                                                                                                                                                                                                                                                            
**Instalación**                                                                                                                                       
Instalar Make si no estan en su computador.                                

Para instalar Make inicie la terminal y escriba lo siguiente:

sudo apt install make

**Ejecutando**                                                                                                                                         
Para abrir el programa necesita ejecutar el archivo make desde la terminal, luego escribir en la terminal lo siguiente: ./Programa
Una vez abierto el programa le pedira ingresar el numero de frases que desea escribir, Luego de escribir las frases el programa le dara el numero de mayusculas y minusculas en esa frase.

**Construido con**                                                                                                                                    
C++
Librerias:
Iostream
List                                                                                                                               

**Versionado**                                                                                                                                        
Version 1.9                                                                                                                                        

**Autores**                                                                                                                                                                                                                                                 
Rodrigo Valenzuela                                                                                                                                                                                               