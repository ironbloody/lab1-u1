#include <iostream>
#include <cctype>
#include "Frases.h"
using namespace std;

int main()
{
  // Se ingresa el numero de frases.
  int tamano;
  cout << "Ingrese el numero de frases que desea escribir: " << endl;
  cin >> tamano;
  
  // Creacion del arreglo con el tamaño.
  Frases arreglo[tamano];
  string frase;
  
  /* For que permite ingresar las 
  frases dependiendo del tamaño del arreglo. */
  for (int i=0; i<tamano; i++) {
      cout << "Ingrese la "<< i+1 << "° frase: " << endl;
      cin.ignore();
      getline(cin, frase);
      // Las frases se guardaran gracias a un set.
      arreglo[i].set_arreglo(frase);
      // Set que cuenta el numero de mayusculas y minusculas.
      arreglo[i].set_upper_and_lower();
      arreglo[i] = arreglo[i].get_arreglo();
    }
  return 0;
}

