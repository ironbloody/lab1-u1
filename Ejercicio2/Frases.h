#include <iostream>
using namespace std;

#ifndef FRASES_H
#define FRASES_H

class Frases {
  
  // Metodos privados.
  private:
    string arreglo = "";
    int upper = 0;
    int lower = 0;
  
  // Metodos publicos.  
  public:
    Frases ();
    Frases (string arreglo);
    
    // Set and Get
    void set_arreglo(string arreglo);
    void set_upper_and_lower();
    string get_arreglo();
};
#endif  