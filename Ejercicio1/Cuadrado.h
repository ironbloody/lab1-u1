#include <iostream>
using namespace std;

#ifndef CUADRADO_H
#define CUADRADO_H

class Cuadrado {
  
  // Metodos privados.
  private:
    int numero = 0;
    int cuadrado = 0;
    
  // Metodos publicos.
  public:
    Cuadrado ();
    Cuadrado (int numero);
    
    // Set and Get
    void set_numero(int numero);
    void set_cuadrado();
    int get_cuadrado();
    
};
#endif  