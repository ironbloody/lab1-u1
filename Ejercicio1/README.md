**Laboratorio 1 Unidad 1**                                                                                             
Programa basado en programación orientada a objetos y arreglos.                                                                           

**Pre-requisitos**                                                                                                                                    
Cplusplus                                                                                                                                                
Make                                                                                                                                             
Ubuntu                                                                                                                                                                                                                                                                            
**Instalación**                                                                                                                                       
Instalar Make si no estan en su computador.                                

Para instalar Make inicie la terminal y escriba lo siguiente:

sudo apt install make

**Ejecutando**                                                                                                                                         
Para abrir el programa necesita ejecutar el archivo make desde la terminal, luego escribir en la terminal lo siguiente: ./Programa
Una vez abierto el programa le pedira ingresar el tamaño del arreglo, una vez ingresado le pedira los numeros y al finalizar le entragara la suma de cuadrados 

**Construido con**                                                                                                                                    
C++
Librerias:
Iostream
List                                                                                                                               

**Versionado**                                                                                                                                        
Version 1.5                                                                                                                                        

**Autores**                                                                                                                                                                                                                                                 
Rodrigo Valenzuela                                                                                                                                                                                               