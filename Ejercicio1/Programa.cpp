#include <iostream>
#include "Cuadrado.h"
using namespace std;

int main()
{
  // Se ingresa el tamaño de arreglo.
  int tamano;
  cout << "Ingrese el tamaño del arreglo: " << endl;
  cin >> tamano;
  
  // Creacion del arreglo con el tamaño.
  int arreglo[tamano];
  int numero;
  Cuadrado c = Cuadrado();
  
  /* For que permite ingresar los numeros 
    de acuerdo el tamaño del arreglo. */
  for (int i=0; i<tamano; i++){
    cout << "Ingrese el numero " << i+1 << ": " << endl;
    cin >> numero;
    // Se guarda el numero
    c.set_numero(numero);
    //set que calcula el cuadrado del numero
    c.set_cuadrado();
    arreglo[i] = c.get_cuadrado();
  }
  
  // For que sumara los cuadrados
  int suma = 0;
  for (int i=0; i<tamano; i++){
    suma = arreglo[i] + suma;
    
  }
  cout << "La suma de los cuadrados es: " << suma << endl;
  
  return numero;
}

